//Require the dev-dependencies
var assert = require('assert');
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../server');
let should = chai.should();

//ChaiHttp for http calls on api.
chai.use(chaiHttp);

describe('Posts', () => {

    /*
    * Test to check if triangle is valid.
    */
    describe('/POST /api/triangle', () => {
        it('Check if triangle is valid', (done) => {
            var triangle = {
                a: -1, b: 20, c: 20
            }
            chai.request(server)
                .post('/api/triangle')
                .send(triangle)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.have.property('type').eql('Invalid');
                    done();
                });
        });
    });


    /*
    * Test to check if triangle is Equilateral.
    */
    describe('/POST /api/triangle', () => {
        it('Check if triangle is equiliteral', (done) => {
            var triangle = {
                a: 20, b: 20, c: 20
            }
            chai.request(server)
                .post('/api/triangle')
                .send(triangle)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.have.property('type').eql('Equilateral');
                    done();
                });
        });
    });


    /*
   * Test to check if triangle is Isosceles.
   */
    describe('/POST /api/triangle', () => {
        it('Check if triangle is Isosceles', (done) => {
            var triangle = {
                a: 10, b: 10, c: 5
            }
            chai.request(server)
                .post('/api/triangle')
                .send(triangle)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.have.property('type').eql('Isosceles');
                    done();
                });
        });
    });


    /*
   * Test to check if triangle is Scalene 
   */
    describe('/POST /api/triangle', () => {
        it('Check if triangle is Scalene', (done) => {
            var triangle = {
                a: 20, b: 5, c: 10
            }
            chai.request(server)
                .post('/api/triangle')
                .send(triangle)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.have.property('type').eql('Scalene');
                    done();
                });
        });
    });


    /*
      * Test the to check getAllPosts method.
      */
    describe('/GET posts', () => {
        it('it should GET all the posts', (done) => {
            chai.request(server)
                .get('/api/posts')
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    done();
                });
        });
    });


    /*
     * Test the to check getAllPosts with comments method.
     */
    describe('/GET postWithcomments', () => {
        it('it should GET all the posts with comments', (done) => {
            chai.request(server)
                .get('/api/postWithcomments')
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    done();
                });
        });
    });




});