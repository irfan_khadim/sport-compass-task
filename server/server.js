require('events').EventEmitter.prototype._maxListeners = 100;

//require express library
const express = require('express');


//require sequalize library for MySql ORM 
sequelize = require('sequelize');

var app = express();

// Require Passport
const passport = require('passport');

// Bring in the routes for the API (delete the default routes)
const routesApi = require('./api/routes/router');

// Static files
app.use(express.static(__dirname + '/uploads'));

// Get server port from environment variable
const port = process.env.SIMPLEX_PORT || 3000;

const server = app.listen(port, function () {
    console.log(`Blog server is starting on port : ${port}`);
});

// JSON Mapping as middleware
app.use(express.json({ limit: '15mb' }));
app.use(express.urlencoded({ limit: '15mb', extended: false }));

// Initialise Passport before using the route middleware
app.use(passport.initialize());

//create a cors middleware
app.use(function (req, res, next) {
    //set headers to allow cross origin request.
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

// Health check with /
app.get('/', function (req, res) {
    res.json({ "ping": "pong" });
});

// Use the API routes when path starts with /api
app.use('/api', routesApi);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers
// Catch unauthorised errors
app.use(function (err, req, res, next) {
    if (err.name === 'UnauthorizedError') {
        res.status(401);
        res.json({ "message": err.name + ": " + err.message });
    }
});

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

server.on('error', onError);

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
    if (error.syscall !== 'listen') {
        throw error;
    }

    var bind = typeof port === 'string'
        ? 'Pipe ' + port
        : 'Port ' + port;

    // handle specific listen errors with friendly messages
    switch (error.code) {
        case 'EACCES':
            console.error(bind + ' requires elevated privileges');
            process.exit(1);
            break;
        case 'EADDRINUSE':
            console.error(bind + ' is already in use');
            process.exit(1);
            break;
        default:
            throw error;
    }
}

process.on('warning', function (e) {
    console.warn(e.stack);
});

process.on('SIGTERM', function () {
    console.log('Closing http server.');
    server.close(function () {
        console.log('Http server closed.');
    });
});
 
module.exports = app; // for testing