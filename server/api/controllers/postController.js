var db = require('../services/database');
var Post = require('../models/post');
var Comment = require('../models/comment');

//require multer for the file uploads
var Multer = require('multer');
var path = require('path');
var fs = require('fs');
const uuidv1 = require('uuid/v1');

var pictureId;

// set the directory for the uploads to the uploaded to
var StorageDir = './uploads/images';
//define the type of upload multer would be doing and pass in its destination, in our case, its a single file with the name photo

const storage = Multer.diskStorage({
    destination: function (req, file, callback) {
        callback(null, StorageDir);
    },
    filename: function (req, file, callback) {
        pictureId = uuidv1() + path.extname(file.originalname);
        callback(null, pictureId);
    }
});

// File Size limit 10MB
const limits = { fileSize: 10 * 1024 * 1024 };

var upload = Multer({
    storage: storage,
    limits: limits,
    fileFilter: function (req, file, cb) {
        cb(null, true)
    },
}).single('post_image');

module.exports.uploadFile = function (req, res) {
    upload(req, res, function (err) {
        if (err) {
            // An error occurred when uploading
            console.log(err);
            if (err.code == "LIMIT_FILE_SIZE") {
                console.log(err);
                return res.status(701).json({ isPosted: false, message: "Maximum limit for file upload is 10MB." })
            } else {
                console.log(err);
                return res.status(500).json({ isPosted: false, message: "Error while uploading a file." });
            }
        }

        if (!req.body.title || !req.body.detail) {
            res.json({ isPosted: false, message: 'Please provide a title and detail for the post.' });
        } else {
            var newPost = {
                title: req.body.title,
                detail: req.body.detail,
                image: pictureId,
                isActive: 1
            };

            db.sync().then(function () {
                return Post.create(newPost).then(function () {
                    res.status(201).json({ isPosted: true, message: 'Post created!' });
                });
            }).catch(function (error) {
                console.log(error);
                res.status(403).json({ isPosted: false, message: 'Error while creating a post.' });
            });
        }
    });
};


module.exports.createPost = function (req, res) {

    if (!req.body.title || !req.body.detail) {
        res.json({ isPosted: false, message: 'Please provide a title and detail for the post.' });
    } else {
        var newPost = {
            title: req.body.title,
            detail: req.body.detail,
            isActive: 1
        };


        db.sync().then(function () {
            return Post.create(newPost).then(function () {
                res.status(201).json({ isPosted: true, message: 'Post created!' });
            });
        }).catch(function (error) {
            console.log(error);
            res.status(403).json({ isPosted: false, message: 'Error while creating a post.' });
        });
    }

}

module.exports.getAllPosts = function (req, res) {
    db.sync().then(function () {

        Post.findAll({
            where: {
                isActive: 1 // where all isActive=1 (isActive will be 0 if post is deleted.)
            }
        }).then(posts => {
            res.status(200).json(posts);
        });

    }).catch(function (error) {
        console.log(error);
        res.status(403).json({ message: 'Some error while retrieving posts.' });
    });
}

module.exports.getPostById = function (req, res) {

    if (!req.query.id) {
        res.json({ message: 'Invalid parameters given.' });
    } else {
        db.sync().then(function () {
            Post.findOne({
                where: {
                    id: req.query.id,
                    isActive: 1 // where all isActive=1 (isActive will be 0 if post is deleted.)
                }
            }).then(posts => {
                res.status(200).json(posts);
            });

        }).catch(function (error) {
            console.log(error);
            res.status(403).json({ message: 'Some error while retrieving posts.' });
        });
    }
}

module.exports.updatePost = function (req, res) {
    if (req.body.id && req.body.title && req.body.detail) {
        Post.update(
            { title: req.body.title, detail: req.body.detail }, //what going to be updated
            { where: { id: req.body.id } } // where clause
        ).then(result => {
            res.status(200).json({ isUpdated: true, message: "Post Updated successfully." });
        }).catch(error => {
            console.log(error);
            res.status(200).json({ message: "Error while updating post." });
        });
    } else {
        res.status(400).json({ message: "Invalid parameters." });
    }
}

module.exports.deletePost = function (req, res) {
    if (req.body.id) {
        Post.findOne({
            where: {
                id: req.body.id
            }
        }).then(post => {
            if (post) {
                post.destroy().then(function (rowDeleted) { // rowDeleted will return number of rows deleted
                    if (rowDeleted) {
                        if (post.image != null) {
                            var imagePath = StorageDir + '/' + post.image;
                            // delete image of post from server. 
                            fs.unlink(imagePath, function (err) {
                                if (err) {
                                    console.log(err);
                                }
                            });
                        }
                        res.status(200).json({ isDeleted: true, message: "Post Deleted successfully." });
                    } else {
                        console.log(err);
                        res.status(500).json({ isDelete: false, message: "Error while deleting a post." });
                    }
                }, function (err) {
                    console.log(err);
                    res.status(500).json({ isDelete: false, message: "Error while deleting a post." });
                });
            } else {
                res.status(200).json({ message: "Post does not exist." });
            }
        });


    } else {
        res.status(400).json({ message: "Invalid parameters." });
    }



}

module.exports.getAllPostWithComments = function (req, res) {

    db.sync().then(function () {

        Post.findAll({
            include: [{
                model: Comment,
                as: 'Comments' // specifies how we want to be able to access our joined rows on the returned data
            }]
        }).then(posts => {
            res.status(200).json(posts);
        });

    }).catch(function (error) {
        console.log(error);
        res.status(403).json({ message: 'Some error while retrieving posts.' });
    });
}