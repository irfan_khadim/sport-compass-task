
/**
 * Handles the post request to the triangle endpoint of the api.
 */
module.exports.isTrianlge = async function (req, res) {

    let sideA = 0, sideB = 0, sideC = 0;

    //Get sides of triangle from the request body.
    sideA = req.body.a;
    sideB = req.body.b;
    sideC = req.body.c;

    if (Number(sideA) && Number(sideB) && Number(sideC)) {

        let triangleType = '';

        if (sideA <= 0 || sideB <= 0 || sideC <= 0) {
            //if any of the side is < 0 than triangle is invalid.
            triangleType = "Invalid";

        } else if (sideA == sideB && sideB == sideC) {
            // If all sides are equal than triangle is Equilateral.
            triangleType = "Equilateral";

        } else if (sideA == sideB || sideA == sideC || sideB == sideC) {
            // If any two sides are equal than triangle is Isosceles.
            triangleType = "Isosceles";
        } else {
            // If none sides are equal than triangle is Scalene.
            triangleType = "Scalene";
        }

        //send triangle type as response to the client using json object.
        res.status(200).json({ type: triangleType });

    } else {
        //if any o
        res.status(200).json({ message: "Please enter valid parameter for side a,b and c of triangle." });
    }

}