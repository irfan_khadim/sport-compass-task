var db = require('../services/database');
var Comment = require('../models/comment');

//Craete a new post and save it in database.
module.exports.createComment = function (req, res) { 

    if (!req.body.postId || !req.body.comment) {
        res.json({ message: 'Invalid parameters given.' });
    } else {
        var newComment = {
            postId: req.body.postId,
            comment: req.body.comment,
            isActive: 1
        };

        db.sync().then(function () {
            return Comment.create(newComment).then(function () {
                res.status(201).json({ isCreated: true, message: 'Comment created!' });
            });
        }).catch(function (error) {
            console.log(error);
            res.status(403).json({ message: 'Error while creating a comment.' });
        });
    }
}

//Get all comments of a post by post id.
module.exports.getCommentsByPostId = function (req, res) {
    if (!req.query.postId) {
        res.json({ message: 'Invalid parameters given.' });
    } else {
        var postId = req.query.postId;
        db.sync().then(function () {

            Comment.findAll({
                where: {
                    postId: postId,
                    isActive: 1
                }
            }).then(comments => {
                res.status(200).json(comments);
            });

        }).catch(function (error) {
            console.log(error);
            res.status(403).json({ message: 'Some error occured.' });
        });
    }
}

module.exports.updateComment = function (req, res) {

    if (req.body.id && req.body.comment) {

        Comment.update(
            { comment: req.body.comment }, //what going to be updated
            {
                where: { id: req.body.id }
            }).then(result => {
                res.status(200).json({ isUpdated: true, message: "Comment Updated successfully." });
            }).catch(error => {
                console.log(error);
                res.status(200).json({ message: "Error while updating comment." });
            });
    } else {
        res.status(400).json({ message: "Invalid parameters." });
    }
}

module.exports.deleteComment = function (req, res) {

    if (req.body.id) {

        Comment.destroy({
            where: { id: req.body.id }
        }).then(deletedRecord => {
            if (deletedRecord === 1) {
                res.status(200).json({ isDeleted: true, message: "Comment Deleted successfully." });
            } else {
                res.status(404).json({ isDeleted: false, message: "Comment not found." })
            }
        }).catch(function (error) {
            res.status(500).json(error);
        });

    } else {
        res.status(400).json({ message: "Invalid parameters." });
    }

}