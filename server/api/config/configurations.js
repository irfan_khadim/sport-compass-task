'use strict';

var config = module.exports;

config.db = {
    user: 'root',
    password: '',
    name: 'blog'
};

config.db.details = {
    host: 'localhost',
    port: 3306,
    dialect: 'mysql',
    logging: false
};

