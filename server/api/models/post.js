var Sequelize = require('sequelize');
var db = require('../services/database');
var Comment = require('./comment');

// The Post model.
'use strict';

//Model schema.
var modelDefinition = {
    title: {
        type: Sequelize.STRING,
        allowNull: false
    },
    detail: {
        type: Sequelize.STRING,
        allowNull: true
    },
    image: {
        type: Sequelize.STRING,
        allowNull: true
    },
    isActive: {
        type: Sequelize.BOOLEAN,
        allowNull: false
    }

};

//Model options.
var modelOptions = {};

//Define the model.
var PostModel = db.define('post', modelDefinition, modelOptions);

// 1 to N relation of post with comments. Post can have many comments.
PostModel.hasMany(Comment, {
    as: 'Comments', onDelete: 'cascade', // delete all comments of the post on deleting a post.
    hooks: true
})

module.exports = PostModel;