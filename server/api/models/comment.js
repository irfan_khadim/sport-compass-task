var Sequelize = require('sequelize');
var db = require('../services/database');

// The Comment model.
'use strict';

//Model schema.
var modelDefinition = {
    comment: {
        type: Sequelize.STRING,
        allowNull: false
    },
    isActive: {
        type: Sequelize.BOOLEAN,
        allowNull: false
    }

};

//model options.
var modelOptions = {};

//Define the model.
var CommentModel = db.define('comment', modelDefinition, modelOptions);

module.exports = CommentModel;