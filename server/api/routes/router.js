const express = require('express');
const router = express.Router();

const controllerTriangle = require('../controllers/triangleController');
const controllerPost = require('../controllers/postController');
const controllerComment = require('../controllers/commentController');

/***
 * Routes for trianlge checker
 */

//Rout to handle post request to check triangle.
router.post('/triangle', controllerTriangle.isTrianlge);


/***
 * Routes for posts
 */

//Rout to upload image
router.post('/upload', controllerPost.uploadFile);

//Rout to create post
router.post('/post', controllerPost.createPost);

router.get('/post', controllerPost.getPostById);

//Rout to get all posts
router.get('/posts', controllerPost.getAllPosts);

//Rout to update a post
router.post('/updatePost', controllerPost.updatePost);

//Rout to delete a post
router.post('/deletePost', controllerPost.deletePost);

//Rout to get posts with comments
router.get('/postWithcomments', controllerPost.getAllPostWithComments);




/***
 * Routes for comments
 */

//Rout to create new comment
router.post('/comment', controllerComment.createComment);

//Rout to get all comments by post id.
router.get('/comments', controllerComment.getCommentsByPostId);

//Rout to update comment.
router.post('/updateComment', controllerComment.updateComment);

//Rout to delete a comment
router.post('/deleteComment', controllerComment.deleteComment);




module.exports = router;