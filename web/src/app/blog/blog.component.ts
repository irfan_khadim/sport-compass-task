import { Component, OnInit } from '@angular/core';//import the file uploader plugin
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';

import { AppService } from '../utility/app.service';

//Api URL for file upload on the server side.
const UPLOAD_URL = "http://localhost:3000/api/upload";

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.css']
})
export class BlogComponent implements OnInit {
  ip = window.location.hostname;
  IMAGES_URL = 'http://' + this.ip + ':3000/images/';

  blogPosts = [];
  comments = [];

  newPost = {
    title: '',
    detail: ''
  }
  comment = {}

  successMsg = "";
  closeResult: string;

  constructor(private appService: AppService) { }

  //declare a property called fileuploader and assign it to an instance of a new fileUploader.
  //pass in the Url to be uploaded to, and pass the itemAlais, which would be the name of the //file input when sending the post request.
  public uploader: FileUploader = new FileUploader({
    url: UPLOAD_URL,
    itemAlias: 'post_image',
    allowedMimeType: ['image/png', 'image/jpeg'],
    additionalParameter: this.newPost
  });
  //This is the default title property created by the angular cli. Its responsible for the app works 

  ngOnInit() {

    //override the onAfterAddingfile property of the uploader so it doesn't authenticate with credentials.
    this.uploader.onAfterAddingFile = (file) => { file.withCredentials = false; };

    //overide the onCompleteItem property of the uploader so we are able to deal with the server response.
    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      console.log("Upload Reponse ", response);
      this.successMsg = "Posted successfully.";
      window.location.reload();
    };

    this.appService.getAll(null, 'postWithcomments').subscribe((resp) => {
      if (resp) {
        this.blogPosts = resp;
      }
    }, (err) => {
      console.error(err);
    });

  }

  createPost() {
    if (this.uploader.queue.length > 0) {
      this.uploader.uploadAll();
    } else {
      this.appService.submit(this.newPost, 'post').subscribe((resp) => {
        if (resp.isPosted) {
          window.location.reload();
        }
      }, (err) => {
        console.error(err);
      });
    }

  }

  deletePost(id) {
    this.appService.submit({ id: id }, 'deletePost').subscribe((resp) => {
      window.location.reload();
    }, (err) => {
      console.error(err);
    });
  }

  createComment(index, postId) {
    this.comment = {
      postId: postId,
      comment: this.comments[index]
    }
    this.appService.submit(this.comment, 'comment').subscribe((resp) => {
      console.log(resp);
      if (resp.isCreated) {
        window.location.reload();
      }
    }, (err) => {
      console.error(err);
    });

  }


  editComment(comment) {
    this.comment = comment;
  }
  deleteComment(id) {
    this.appService.submit({ id: id }, 'deleteComment').subscribe((resp) => {
      if (resp.isDeleted) {
        window.location.reload();
      }
    }, (err) => {
      console.error(err);
    });
  }

  updateComment(comment) {
    this.appService.submit(comment, 'updateComment').subscribe((resp) => {
      if (resp.isUpdated) {
        window.location.reload();
      }
    }, (err) => {
      console.error(err);
    });
  }

  cancelUpdateComment() {
    window.location.reload();
  }




}
