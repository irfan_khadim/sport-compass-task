import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AppSettings } from '../config/settings';
import { NgxSpinnerService } from 'ngx-spinner';

@Injectable({
  providedIn: 'root'
})
// Parent Injectable
@Injectable()
export class AppService {
  [x: string]: any;
  public types: any;

  constructor(private http: HttpClient, private spinner: NgxSpinnerService) {
  }

  private request(method, type, interfaceType): Observable<any> {
    this.spinner.show();
    let base;
    if (method === 'post') {
      base = this.http.post(AppSettings.API_ENDPOINT + `${type}`, interfaceType);
    } else if (method === 'delete') {
      base = this.http.delete(AppSettings.API_ENDPOINT + `${type}?id=${interfaceType.id}`);
    } else if (method === 'get') {
      if (interfaceType != null && interfaceType.id != null) {
        base = this.http.get(AppSettings.API_ENDPOINT + `${type}?id=${interfaceType.id}`);
      } else {
        base = this.http.get(AppSettings.API_ENDPOINT + `${type}`);
      }
    }
    return base;
  }

  public getToken(): string {
    return localStorage.getItem('token');
  }

  public submit(data, _url): Observable<any> {
    return this.request('post', _url, data);
  }

  public update(data, _url): Observable<any> {
    return this.request('post', _url, data);
  }

  public delete(data, _url): Observable<any> {
    return this.request('delete', _url, data);
  }

  public get(id, _url): Observable<any> {
    return this.request('get', _url, id);
  }

  public upload(data, _url): Observable<any> {
    return this.request('post', _url, data);
  }

  public getAll(id, _url): Observable<any> {
    return this.request('get', _url, id);
  }

  //get 24 hour format time

  get24TimeString(datetime) {
    var timeStr = <Date>datetime.getHours() + ":" + <Date>datetime.getMinutes();
    return timeStr;
  }

}


