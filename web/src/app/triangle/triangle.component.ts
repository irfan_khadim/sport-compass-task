import { Component, OnInit } from '@angular/core';
import { AppService } from '../utility/app.service';

@Component({
  selector: 'app-triangle',
  templateUrl: './triangle.component.html',
  styleUrls: ['./triangle.component.css']
})
export class TriangleComponent implements OnInit {

  triangle = {
    a: 0,
    b: 0,
    c: 0
  };


  result = "";


  constructor(private appService: AppService) { }

  ngOnInit() {
  }

  getResult() {

    this.appService.submit(this.triangle, 'triangle').subscribe((resp) => {
      console.log("Triangle Response : ", resp);
      if (resp.type) {
        this.result = resp.type;
      } else {
        this.result = resp.message;
      }
    }, (err) => {
      console.error(err);
    });
  }

  reset() {
    this.triangle.a = 0;
    this.triangle.b = 0;
    this.triangle.c = 0;
    this.result = "";
  }

}
