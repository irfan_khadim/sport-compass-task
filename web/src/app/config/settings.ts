var ip = window.location.hostname;
var protocol = window.location.protocol == "https:" ? 'https' : 'http';
var baseURL = window.location.origin;
export class AppSettings {
    public static API_ENDPOINT = `${protocol}://${ip}:3000/api/`;
    public static API_SOCKET = `${protocol}://${ip}:3000/updates`;
    public static IMAGES_URL = `${protocol}://${ip}:3000/images/`;
    public static FORM_UPLOADS_URL = `${protocol}://${ip}:3000/formuploads/`;
    public static MAX_IMAGE_SIZE = 5 * 1024 * 1024;
    public static DEFAULT_PROFILE_PICTURE = '../../assets/img/avatar-1.jpg';
    public static SURVEY_URL = `${baseURL}/#/survey`;
    public static FORMAT = 'MMM dd, yyy HH:mm';
    public static DATE_FORMATE_SHORT = 'MMM dd, yyy';
    

}
