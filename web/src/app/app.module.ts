import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { NgxSpinnerModule } from 'ngx-spinner';
import { FileUploadModule } from 'ng2-file-upload';
import { ModalModule } from 'ngx-bootstrap';
import { AppRoutingModule } from './app-routing.module';

// App Components
import { AppComponent } from './app.component';

// App Services
import { AppService } from './utility/app.service';
import { TriangleComponent } from './triangle/triangle.component';
import { BlogComponent } from './blog/blog.component';
import { RouterService } from './utility/router.service';

@NgModule({
  declarations: [
    AppComponent,
    TriangleComponent,
    BlogComponent
  ],
  imports: [
    FormsModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgxSpinnerModule,
    FileUploadModule,
    ModalModule.forRoot(),
  ],
  providers: [AppService, RouterService],
  bootstrap: [AppComponent]
})
export class AppModule { }
