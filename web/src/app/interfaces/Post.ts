export interface Post {
    id?: string;
    title: string;
    detail: string;
    image?: any;
    isActive: boolean;
    createdAt?: Date;
}