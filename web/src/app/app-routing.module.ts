import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TriangleComponent } from './triangle/triangle.component';
import { BlogComponent } from './blog/blog.component';

const routes: Routes = [
  // Triangle Mapping   
  { path: 'triangle', component: TriangleComponent },

  // Blog Mapping
  { path: 'blog', component: BlogComponent },


];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    scrollPositionRestoration: 'enabled'  // Add options right here
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }